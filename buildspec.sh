#!/usr/bin/env bash
# WATCH OUT: the .phases.build.commands[]
# are seemingly each executed in a *SEPARATE* /bin/bash -c type thing
# and it does not run in "set -e" mode by default

if [[ -z "${JDK:-}" ]]; then
  echo "JDK env-var is not set" >&2
  exit 1
fi

ECR_DOCKER_IMAGE_BASE="${AWS_ECR_REGISTRY}/${CI_PROJECT_PATH}"

# allegedly a local builder: https://docs.aws.amazon.com/codebuild/latest/userguide/use-codebuild-agent.html
# https://docs.aws.amazon.com/codebuild/latest/userguide/build-spec-ref.html

CI_RUNNER_EXECUTABLE_ARCH="linux/$(go env GOARCH)"
# CI_RUNNER_EXECUTABLE_ARCH is of the form 'linux/amd64'
# but we want it 'linux-amd64' for both ecr and optionally buildx
CI_RUNNER_ARCH_DASH=$(echo "$CI_RUNNER_EXECUTABLE_ARCH" | sed -E 's@/@-@g')

# their container images don't ship with amazon-ecr-credential-helper by default
# find /bin /usr/bin /usr/local/bin -name "*credential-helper*" -exec ls -la '{}' ';'
if true; then
  aws --region "$AWS_DEFAULT_REGION" ecr get-login-password \
    | docker login --username AWS --password-stdin "$AWS_ECR_REGISTRY"
else
  # we'd need to patch in the $HOME/.docker/config.json and can't be bothered
  AWS_ECR_LOGIN_VERSION=0.7.1
  # this URL comes from https://github.com/awslabs/amazon-ecr-credential-helper/releases/tag/v0.7.1
  ecr_login_bin="https://amazon-ecr-credential-helper-releases.s3.us-east-2.amazonaws.com/${AWS_ECR_LOGIN_VERSION}/${CI_RUNNER_ARCH_DASH}/docker-credential-ecr-login"
  curl -fsSL -o /usr/local/bin/docker-credential-ecr-login "$ecr_login_bin"
  chmod +x      /usr/local/bin/docker-credential-ecr-login
fi

my_docker() { env DOCKER_BUILDKIT=1 docker --log-level=debug "$@"; }
my_docker version

echo 'BUILDX-BEFORE:'
docker info -f '{{ range .ClientInfo.Plugins }}{{ print .Path "\t" .Version "\n" }}{{ end }}'
# they ship with /usr/local/lib/docker/cli-plugins/docker-buildx v0.11.0

WANT_BUILDX_VER=v0.12.1
buildx_url="https://github.com/docker/buildx/releases/download/${WANT_BUILDX_VER}/buildx-${WANT_BUILDX_VER}.${CI_RUNNER_ARCH_DASH}"
mkdir -p    "$HOME/.docker/cli-plugins"
curl -fsSLo "$HOME/.docker/cli-plugins/docker-buildx" "$buildx_url"
chmod 0755  "$HOME/.docker/cli-plugins/docker-buildx"
echo 'BUILDX-AFTER:'
docker info -f '{{ range .ClientInfo.Plugins }}{{ print .Path "\t" .Version "\n" }}{{ end }}'

# see: https://gitlab.com/gitlab-org/gitlab/-/issues/339567 and likely a ton more
# also, as the tag implies, this actually *bundles* qemu in the image,
# and those binaries get mmaped into the runner's kernel
# which explains why subsequent jobs don't need access to qemu-aarch64 binaries
# https://hub.docker.com/r/multiarch/qemu-user-static/tags
# while, yes, the 7.2.0-1 version bundles way too many binaries with it,
# the alternative is to reference just the architectures one wishes
# *AND* the :register image for the magic shell script; using the versioned tag is the "one stop shopping"
my_docker run --privileged --name binfmt-patcher --entrypoint /bin/sh \
  'docker.io/multiarch/qemu-user-static:7.2.0-1' \
  -exc '/register --reset --persistent yes'

if [[ -n "${TRACE:-}" ]]; then
  my_docker buildx version
  my_docker buildx create --name mybuilder --use
  my_docker buildx inspect --bootstrap
  my_docker buildx ls
  my_docker ps -a
  my_docker exec buildx_buildkit_mybuilder0 buildctl debug workers --format '{{json .}}' | jq
  my_docker exec buildx_buildkit_mybuilder0 buildctl debug info --format '{{json .}}' | jq
  my_docker exec buildx_buildkit_mybuilder0 ps auwx
  my_docker exec buildx_buildkit_mybuilder0 sh -c 'find /usr/bin -type f'
fi

if [[ -n "${CI_TRACE_DEBUG:-}" ]]; then
  for i in /codebuild/readonly/bootstrap-log \
    /codebuild/readonly/bin/ip_address.txt \
    /codebuild/readonly/executor-log \
    /codebuild/readonly/build-info.json \
    /codebuild/readonly/variables.json \
    /codebuild/bootstrap/start \
    /codebuild/output/tmp/env.sh \
    /codebuild/output/tmp/dir.txt \
    /codebuild/output/tmp/script.sh \
    /codebuild/global.json
  do
    echo "<editor-fold name='$i'>"
    gzip -9 < "$i" | base64
    echo "</editor-fold>"
  done
  set | gzip -9 | base64
  for _ in $(seq 1 10); do
    echo "DYING DUE TO DEBUG SET" >&2
  done
  exit 1
fi

# I'm aware of how weird this is: their "Source.Location" expects to be _wired up_
# to GitHub or GHE, and doesn't really understand "look, friend, just clone the repo"
git clone https://gitlab.com/openraven/open/orvn-java.git
cd orvn-java || exit 1

# BE SURE TO DO ARM FIRST so if it's wrong it'll die early
for PLAT in arm64 amd64; do
  BUILD_PLATFORM="linux/${PLAT}"

  # this nonsense is because of
  # 429 Too Many Requests - Server message: toomanyrequests: You have reached your pull rate limit
  curl -fsSLo "Dockerfile.${JDK}" "https://github.com/zulu-openjdk/zulu-openjdk/raw/master/ubuntu/${JDK}-latest/Dockerfile"
  # https://gallery.ecr.aws/docker/library/ubuntu
  sed -i.bak -e '/^FROM ubuntu/s@^FROM ubuntu@FROM public.ecr.aws/docker/library/ubuntu@' "Dockerfile.${JDK}"

  openjdk_image_tag="${ECR_DOCKER_IMAGE_BASE}:zulu-openjdk-${JDK}"
  # https://docs.docker.com/build/exporters/local-tar/
  # ok, sorry, it seems we just HAVE to push this image: https://github.com/docker/buildx/issues/847
  docker buildx build --platform "$BUILD_PLATFORM" \
    --push --progress plain \
    --tag "${openjdk_image_tag}" -f "Dockerfile.${JDK}" .

  sed -i.bak -e '/^FROM /s@^FROM .*@FROM '"${openjdk_image_tag}"'@' Dockerfile

  BUILD_ARG="ZULU_OPENJDK_VERSION=${JDK}"
  BASE_TAG="${JDK}" # the -PLAT is implied
  ECR_DOCKER_IMAGE="${ECR_DOCKER_IMAGE_BASE}:${BASE_TAG}"
  echo "cooking ${ECR_DOCKER_IMAGE} ($BUILD_PLATFORM) ..."
  my_docker buildx build --platform $BUILD_PLATFORM \
      --build-arg "TARGETARCH=$PLAT" \
      --build-arg "$BUILD_ARG" \
      --tag "${ECR_DOCKER_IMAGE}-${PLAT}" \
      --push --progress plain --provenance=false .
done
my_docker manifest create "$ECR_DOCKER_IMAGE" "${ECR_DOCKER_IMAGE}-amd64" "${ECR_DOCKER_IMAGE}-arm64"
my_docker manifest annotate --arch amd64 "$ECR_DOCKER_IMAGE" "${ECR_DOCKER_IMAGE}-amd64"
my_docker manifest annotate --arch arm64 "$ECR_DOCKER_IMAGE" "${ECR_DOCKER_IMAGE}-arm64"
my_docker manifest push "$ECR_DOCKER_IMAGE"
