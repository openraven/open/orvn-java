ARG ZULU_OPENJDK_VERSION
FROM docker.io/azul/zulu-openjdk:${ZULU_OPENJDK_VERSION}

EXPOSE 80

# one can track this URL via "brew cat jmxterm" or https://github.com/jiaqi/jmxterm/releases
ARG JMXTERM_VER=1.0.4
ADD https://github.com/jiaqi/jmxterm/releases/download/v${JMXTERM_VER}/jmxterm-${JMXTERM_VER}-uber.jar \
    /root/jmxterm.jar

RUN set -e ;\
    export DEBIAN_FRONTEND=noninteractive ;\
    apt-get update ;\
    apt-get install -y curl jq unzip ;\
    apt-get clean ;\
    rm -rf /var/lib/apt/lists/*

ARG TARGETARCH
ARG ARCH=x86_64
RUN set -e ;\
    # https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html#cliv2-linux-install \
    [ "${TARGETARCH}" = "arm64" ] && ARCH=aarch64 ;\
    curl -fsSLo /tmp/awscliv2.zip https://awscli.amazonaws.com/awscli-exe-linux-${ARCH}.zip ;\
    unzip -q    /tmp/awscliv2.zip -d /tmp ;\
    /tmp/aws/install --install-dir /opt/aws-cli --bin-dir /usr/bin ;\
    rm -rf /tmp/aws ;\
    aws sts assume-role --generate-cli-skeleton

ENTRYPOINT ["java", "-jar", "/root/service.jar"]
