#!/usr/bin/env python3
# coding=utf-8
import json
import yaml
from typing import Any, Dict, List

with open("buildspec.template.yml") as fh:
    result = yaml.safe_load(fh.read())
with open("buildspec.sh") as fh:
    buildspec = fh.read()

clone_env: Dict[str, Any] = result["batch"]["build-graph"][0]["env"]
build_graph: List[Dict] = []

jdk_list = ["11", "17", "21"]

for jdk in jdk_list:
    my_env = dict(clone_env)
    my_env["variables"] = {
        "JDK": jdk
    }
    item = {
        # BATCH_SPEC_VALIDATION_ERROR: BatchSpec has invalid values:
        # must contain only alphanumeric characters and underscores and be less than 128 characters in length.
        "identifier": f"both_{jdk}",
        # I doubt our buildspec friend tolerates anchors ...
        "env": json.loads(json.dumps(my_env)),
    }
    build_graph.append(item)
    del item

result.update({
    "batch": {
        "build-graph": build_graph
    },
    "phases": {
        "build": {
            "commands": buildspec
        }
    }
})
buildspec_yaml: str = yaml.safe_dump(result)
with open("buildspec.yml", "w") as fh:
    fh.write(buildspec_yaml)

with open("codebuild.template.yml") as fh:
    cfn = yaml.safe_load(fh.read())
cfn["Resources"]["project"]["Properties"]["Source"]["BuildSpec"] = buildspec_yaml
with open("codebuild.cfn.yml", "w") as fh:
    fh.write(yaml.safe_dump(cfn))
